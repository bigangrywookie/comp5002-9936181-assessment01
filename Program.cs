﻿using System;

namespace exercise_18
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            /*Start
            -declare variables first 
            -Print welcoming message "welkom op de taart Brett's winkel" to screen
            -print "first, please type your name" to screen
            -read user entry and store in variable "name"
            -print "Thank you for coming "name"
            -print "Please enter a price with 2 decimal places e.g. $2.50"
            -store the user entry in the variable price using double.parse .. this converts the user input to a double variable
            -print "great! thank you" to the screen
            -print "would you like to add another price? .....y/n?" to the screen
            -store the user input to variable "option"
            -set if commands for variable "option"
            -if user input is y, then print "Please enter a price with 2 decimal places e.g. $2.50"
            -store variable and add the previous variable with price +=double.parse
            -if user input iS n, then print $"your total is {price}" to screen
            -print $"your total including gst is {price *1.15} ... this takes the total of the price variables and adds 15% 
            and displays it to the screen
            -print "thankyou for shopping with us please come again" to the screen
            End*/
            Console.Clear();
            var option = "";
            var price = 0.00;
            var name = "";
            


            System.Console.WriteLine("");
            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Console.WriteLine("@@   Welkom op de taart Brett's winkel  @@");
            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");


            Console.WriteLine("First, please type your name");
            name = Console.ReadLine();
            Console.WriteLine($"Thank you for coming {name}.");
            Console.WriteLine("please enter a price with 2 decimal places e.g. $2.50");
            price = double.Parse(Console.ReadLine());
            Console.WriteLine("Great! thank you");
            Console.WriteLine("would you like to add another price? ..... y/n?");
            option = Console.ReadLine();
            if(option=="y")
            {
                Console.WriteLine("please enter a price with 2 decimal places e.g. $2.50");
                price += double.Parse(Console.ReadLine());
            }
            else if(option=="n")
            {
                 Console.WriteLine ($"your total is {price}");
            }
            Console.WriteLine($"your total including gst is {price *1.15}");

            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Console.WriteLine("@@    Thank you for shopping with us    @@");
            Console.WriteLine("@@           Please come again!         @@");
            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}